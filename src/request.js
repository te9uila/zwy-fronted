import axios from "axios";

const request = axios.create({
    baseURL: 'http://localhost:10086/model',
    timeout: 10000,
});

export default request;