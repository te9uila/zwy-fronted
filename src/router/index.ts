import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/loginPage/vue/login.vue')
    }, {
      path: '/register',
      name: 'register',
      component: () => import('../views/loginPage/vue/register.vue')
    },{
      path: '/layOut',
      name: 'layOut',
      component: () => import('../views/mainPages/layOut/layOut.vue'),
      redirect: '/home',
      children:[
        {
          path: '/home',
          name: 'home',
          component: () => import('../views/mainPages/home/home.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG001',
          name: 'YG001',
          component: () => import('../views/mainPages/YG001/YG001.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG001-s01',
          name: 'YG001-s01',
          component: () => import('../views/mainPages/YG001/YG001-s01.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG001-s02',
          name: 'YG001-s02',
          component: () => import('../views/mainPages/YG001/YG001-s02.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG002',
          name: 'YG002',
          component: () => import('../views/mainPages/YG002/YG002.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG003',
          name: 'YG003',
          component: () => import('../views/mainPages/YG003/YG003.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG004',
          name: 'YG004',
          component: () => import('../views/mainPages/YG004/YG004.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG005',
          name: 'YG005',
          component: () => import('../views/mainPages/YG005/YG005.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG006',
          name: 'YG006',
          component: () => import('../views/mainPages/YG006/YG006.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },{
          path: '/YG007',
          name: 'YG007',
          component: () => import('../views/mainPages/YG007/YG007.vue'),
          beforeEnter: (to, form, next) => {
            if(!sessionStorage.getItem('c_user')){
              next('login');
            }else{
              next();
            }
          }
        },
      ]
    }
  ]
})

export default router
