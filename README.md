## 前端开发模板

### 基础组件

### 	`vue3 + element-plus + anxios`

### 前置条件

1. node.js 版本 `v16.20.x`
2. vue 脚手架 版本 `@vue/cli 5.0.8`
3. vue 版本 `vue@3.4.15`

### 查看版本方法

1. node.js : `node -v`
2. vue 脚手架 : `vue -V`
3. vue 版本 : `npm list vue`

### 部署方法

1. 从`gitlab`上拉下代码
2. 打开项目, 运行`npm install`安装响应依赖
3. `npm run dev`尝试运行项目
4. `http://127.0.0.1:5173/test`查看效果正常, 则部署成功
